package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
	"testing"
	"time"
)

type Telemetry struct {
	Timestamp    time.Time
	SatelliteID  int
	RedHighLimit int
	YellowHigh   int
	YellowLow    int
	RedLowLimit  int
	RawValue     float64
	Component    string
}

type Alert struct {
	SatelliteID int
	Severity    string
	Component   string
	Timestamp   time.Time
}

func main() {
	file, err := os.Open("telemetry.txt")
	if err != nil {
		fmt.Println("Error opening file:", err)
		return
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	var telemetryList []*Telemetry

	for scanner.Scan() {
		line := scanner.Text()
		fields := strings.Split(line, "|")

		timestamp, _ := time.Parse("20060102 15:04:05.000", fields[0])
		satelliteID, _ := strconv.Atoi(fields[1])
		redHighLimit, _ := strconv.Atoi(fields[2])
		yellowHigh, _ := strconv.Atoi(fields[3])
		yellowLow, _ := strconv.Atoi(fields[4])
		redLowLimit, _ := strconv.Atoi(fields[5])
		rawValue, _ := strconv.ParseFloat(fields[6], 64)
		component := fields[7]

		telemetry := &Telemetry{
			Timestamp:    timestamp,
			SatelliteID:  satelliteID,
			RedHighLimit: redHighLimit,
			YellowHigh:   yellowHigh,
			YellowLow:    yellowLow,
			RedLowLimit:  redLowLimit,
			RawValue:     rawValue,
			Component:    component,
		}

		telemetryList = append(telemetryList, telemetry)
	}

	alerts := findAlerts(telemetryList)

	for _, alert := range alerts {
		fmt.Println(alert)
	}
}

func findAlerts(telemetryList []*Telemetry) []*Alert {
	alerts := []*Alert{}

	for i := 0; i < len(telemetryList)-2; i++ {
		// Check for three consecutive battery voltage readings under red low limit within 5 minutes
		if telemetryList[i].SatelliteID == telemetryList[i+1].SatelliteID && telemetryList[i+1].SatelliteID == telemetryList[i+2].SatelliteID {
			if float64(telemetryList[i].RedLowLimit) > telemetryList[i].RawValue &&
				float64(telemetryList[i+1].RedLowLimit) > telemetryList[i+1].RawValue &&
				float64(telemetryList[i+2].RedLowLimit) > telemetryList[i+2].RawValue {
				alerts = append(alerts, &Alert{
					SatelliteID: telemetryList[i].SatelliteID,
					Severity:    "RED LOW",
					Component:   telemetryList[i].Component,
					Timestamp:   telemetryList[i].Timestamp,
				})
			}
		}

		// Check for three consecutive thermostat readings exceeding red high limit within 5 minutes
		if telemetryList[i].SatelliteID == telemetryList[i+1].SatelliteID && telemetryList[i+1].SatelliteID == telemetryList[i+2].SatelliteID {
			if float64(telemetryList[i].RedHighLimit) < telemetryList[i].RawValue &&
				float64(telemetryList[i+1].RedHighLimit) < telemetryList[i+1].RawValue &&
				float64(telemetryList[i+2].RedHighLimit) < telemetryList[i+2].RawValue {
				alerts = append(alerts, &Alert{
					SatelliteID: telemetryList[i].SatelliteID,
					Severity:    "RED HIGH",
					Component:   telemetryList[i].Component,
					Timestamp:   telemetryList[i].Timestamp,
				})
			}
		}
	}

	return alerts
}

func TestFindAlerts(t *testing.T) {
	// Sample telemetry data for testing
	telemetryList := []*Telemetry{
		{Timestamp: time.Now(), SatelliteID: 1, RedLowLimit: 10, RawValue: 5.0, Component: "Battery Voltage"},
		{Timestamp: time.Now(), SatelliteID: 1, RedLowLimit: 10, RawValue: 4.0, Component: "Battery Voltage"},
		{Timestamp: time.Now(), SatelliteID: 1, RedLowLimit: 10, RawValue: 3.0, Component: "Battery Voltage"},
		{Timestamp: time.Now(), SatelliteID: 2, RedHighLimit: 30, RawValue: 32.0, Component: "Thermostat"},
		{Timestamp: time.Now(), SatelliteID: 2, RedHighLimit: 30, RawValue: 33.0, Component: "Thermostat"},
		{Timestamp: time.Now(), SatelliteID: 2, RedHighLimit: 30, RawValue: 34.0, Component: "Thermostat"},
	}

	// Call the function being tested
	alerts := findAlerts(telemetryList)

	// Check if the expected number of alerts were generated
	expectedAlerts := 2
	if len(alerts) != expectedAlerts {
		t.Errorf("Expected %d alerts, got %d", expectedAlerts, len(alerts))
	}

	// Additional checks can be added to validate the content of the alerts
}
